﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;

namespace DroneTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n=== WELCOME TO HARÃ'S DRONE TEST ===\n");
            Console.Write("\nPLEASE INFORM THE DRONE NAME AND THE WEIGHT THAT IT CAN CARRY.");
            Console.Write("\nTHE MAXIMUM NUMBER OF DRONES IS 100.");

            Console.Write("\n\n==============================================================");
            Console.Write("\nLET'S START WITH THE DRONES:");
            Console.Write("\n==============================================================");

            // FOR TESTING PURPOSES

            //var droneList = new List<Drone>();
            //var locationList = new List<Location>();

            //AddMockup(droneList, locationList);

            // ADDING DRONES

            int currentDroneNumber = 1;
            var droneList = new List<Drone>();

            while (currentDroneNumber <= 100)
            {
                if (currentDroneNumber > 1)
                {
                    string droneAnswer = "";
                    while (droneAnswer != "Y" && droneAnswer != "N")
                    {
                        Console.Write("\nAdd new drone? (Y/N)");
                        droneAnswer = Console.ReadLine().ToUpper();
                    }

                    if (droneAnswer == "N")
                        break;
                }

                droneList.Add(CreateDrone(currentDroneNumber));
                currentDroneNumber++;
            }

            // ADDING LOCATIONS

            string locationAnswer = "";
            int currentLocationNumber = 1;
            var locationList = new List<Location>();

            Console.Write("\n\n==============================================================");
            Console.Write("\nNOW IT'S TIME FOR THE LOCATIONS:");
            Console.Write("\n==============================================================");

            CreateLocation(currentLocationNumber);
            currentLocationNumber++;

            while (locationAnswer != "N")
            {
                Console.Write("\nAdd new trip? (Y/N)");
                locationAnswer = Console.ReadLine().ToUpper();

                if (locationAnswer == "Y")
                {
                    locationList.Add(CreateLocation(currentLocationNumber));
                    currentLocationNumber++;
                };
            }

            CalculateTrips(droneList, locationList);
        }

        public static Drone CreateDrone(int currentDroneNumber)
        {
            var drone = new Drone();

            Console.Write($"\n\nInsert drone #{currentDroneNumber} name:\n");
            drone.Name = Console.ReadLine();

            int maximumWeight = 0;
            bool success = false;

            while (!success)
            {
                Console.Write($"\nInsert drone '{drone.Name}' maximum weight (must be an integer):\n");
                string maximumWeightText = Console.ReadLine();

                success = int.TryParse(maximumWeightText, out maximumWeight);
            }

            drone.MaximumWeight = maximumWeight;

            return drone;
        }
        public static Location CreateLocation(int currentLocationNumber)
        {
            var location = new Location();

            Console.Write($"\n\nInsert the location #{currentLocationNumber} name:\n");
            location.Name = Console.ReadLine();

            int weight = 0;
            bool success = false;

            while (!success)
            {
                Console.Write($"\nInsert the package weight for the location #{currentLocationNumber} (must be an integer):\n");
                string weightText = Console.ReadLine();

                success = int.TryParse(weightText, out weight);
            }

            location.Weight = weight;

            return location;
        }
        public static void CalculateTrips(List<Drone> droneList, List<Location> locationList)
        {
            // Just for fun ;D
            DisplayFunnyMessages();

            var tripList = new List<Trip>();
            var orderedDroneList = droneList.OrderByDescending(a => a.MaximumWeight).ToList();

            foreach (var drone in orderedDroneList)
            {
                var trip = new Trip();
                int droneMaximumWeightLeft = drone.MaximumWeight;

                trip.Drone = drone;
                trip.Locations = new List<Location>();

                foreach (var location in locationList.ToList())
                {
                    if (droneMaximumWeightLeft > 0)
                    {
                        var closestLocation = locationList.OrderBy(x => Math.Abs(x.Weight - droneMaximumWeightLeft)).FirstOrDefault();

                        if (closestLocation != null && closestLocation.Weight <= droneMaximumWeightLeft)
                        {
                            droneMaximumWeightLeft -= closestLocation.Weight;

                            trip.Locations.Add(closestLocation);
                            locationList.Remove(closestLocation);
                        }
                    }
                }

                tripList.Add(trip);
            }

            var tripListWithLocations = tripList.Where(a => a.Locations.Any());

            if (!tripListWithLocations.Any())
            {
                Console.Write($"\n\n    >> There are no matching trips for any of your drones. Sorry :( <<\n");
            }
            else
            {
                foreach (var trip in tripListWithLocations)
                {
                    Console.Write($"\n- Drone: {trip.Drone.Name}");
                    Console.Write($"\n    - Maximum Weight: {trip.Drone.MaximumWeight}");
                    Console.Write($"\n\n- Trip");

                    decimal tripTotalWeigth = 0;

                    foreach (var location in trip.Locations)
                    {
                        tripTotalWeigth += location.Weight;

                        Console.Write($"\n    - Location: {location.Name}");
                        Console.Write($"\n    - Weight: {location.Weight}");
                        Console.Write($"\n    - Location drone load: {decimal.Round((decimal)location.Weight / trip.Drone.MaximumWeight * 100, 2, MidpointRounding.AwayFromZero)}%\n");
                    }

                    Console.Write($"\n    - Trip total weight: {tripTotalWeigth}");
                    Console.Write($"\n    - Trip drone load: {decimal.Round(tripTotalWeigth / trip.Drone.MaximumWeight * 100, 2, MidpointRounding.AwayFromZero)}%\n\n");
                }
            }

            Console.Write($"\n\n ====== PRESS ANY KEY TO EXIT ======\n");
            Console.ReadLine();
            Environment.Exit(0);
        }
        private static void DisplayFunnyMessages()
        {
            Console.Write("\n\n==============================================================");
            Console.Write("\nJUST A SECOND WHILE I CALCULATE THE TRIPS FOR YOU:");
            Console.Write("\n==============================================================");

            Console.Write("\n\nConnecting...");

            Thread.Sleep(2500);

            Console.Write("\n\nConnected!");

            Thread.Sleep(1000);

            Console.Write("\n\nAccessing top secret network...");

            Thread.Sleep(2500);

            Console.Write("\n\nWe're in!");

            Thread.Sleep(1000);

            Console.Write("\n\nConnecting to NASA's mainframe...");

            Thread.Sleep(2500);

            Console.Write("\n\nConnected!");

            Thread.Sleep(1000);

            Console.Write("\n\nSending data...");

            Thread.Sleep(2500);

            Console.Write("\n\nDone.");

            Thread.Sleep(1000);

            Console.Write("\n\nRetrieving data...");

            Thread.Sleep(2500);

            Console.Write("\n\nHere it is!");
        }
        private static void AddMockup(List<Drone> droneList, List<Location> locationList)
        {
            droneList.Add(new Drone()
            {
                Name = "DRONE 01",
                MaximumWeight = 20
            });

            droneList.Add(new Drone()
            {
                Name = "DRONE 02",
                MaximumWeight = 500
            });

            droneList.Add(new Drone()
            {
                Name = "DRONE 03",
                MaximumWeight = 218
            });

            droneList.Add(new Drone()
            {
                Name = "DRONE 04",
                MaximumWeight = 150
            });

            locationList.Add(new Location()
            {
                Name = "LOCATION 01",
                Weight = 15
            });

            locationList.Add(new Location()
            {
                Name = "LOCATION 02",
                Weight = 2
            });

            locationList.Add(new Location()
            {
                Name = "LOCATION 03",
                Weight = 322
            });

            locationList.Add(new Location()
            {
                Name = "LOCATION 04",
                Weight = 111
            });

            locationList.Add(new Location()
            {
                Name = "LOCATION 05",
                Weight = 17
            });
        }
    }

    public class Drone
    {
        public string Name { get; set; }
        public int MaximumWeight { get; set; }
    }
    public class Location
    {
        public string Name { get; set; }
        public int Weight { get; set; }
    }
    public class Trip
    {
        public Drone Drone { get; set; }
        public List<Location> Locations { get; set; }
    }
}